remote_state {
  backend = "s3"
  config = {
    region  = "us-east-1"
    profile = "default"
    encrypt = true
    key     = "${path_relative_to_include()}/terraform.tfstate"
    bucket  = "example-terraform-state"
  }
}

locals {
    vars = try(
    yamldecode(file("${get_terragrunt_dir()}/common.yaml")),
    yamldecode(file(find_in_parent_folders("common.yaml"))),

  )
}

generate backend {
  path      = "backend-generated.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF

terraform {
  backend "s3" {}
  required_version = "${local.vars.terraform_version}"
}
EOF
}

generate "provider" {
  path      = "providers-generated.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "us-east-1" 
  alias = "us-east-1" 
}


%{if can("${local.vars.aws_regions}") }
%{for region in "${local.vars.aws_regions}"}
provider "aws" {
  region  = "${region}"
  alias   = "${region}"
}
  %{endfor}
%{endif}

EOF
}

generate versions {
  path  = "versions-generated.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  required_version = ">= 1.2.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.67.0"
    }
  }
}
EOF
}




