include "root" {
  path = find_in_parent_folders()
}

locals {
    vars = try(
    yamldecode(file("${get_terragrunt_dir()}/common.yaml")),
    yamldecode(file(find_in_parent_folders("common.yaml"))),

  )

}


generate aws_config {
  path      = "aws-config-generated.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
module "config-base" {
  source = "../../_mod/aws_config/config_base"
  config_bucket = "${local.vars.config_bucket}"
}
module "config-region-${local.vars.aws_region}" {
    source = "../../_mod/aws_config/config_regions"
    config_bucket = "${local.vars.config_bucket}"
    depends_on = [module.config-base]
}
#%{for region in "${local.vars.aws_regions}"}
module "config-region-${region}" {
    source = "../../_mod/aws_config/config_regions"
    providers = { aws = aws.${region}}
    config_bucket = "${local.vars.config_bucket}"
    depends_on = [module.config-base]
}
%{endfor} 
EOF
}