resource "aws_iam_service_linked_role" "config" {
  aws_service_name = "config.amazonaws.com"
  description = "AWS managed service linked IAM role for config service"
}