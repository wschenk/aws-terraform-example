
resource "aws_s3_bucket" "example_config_bucket" {
  bucket     = var.config_bucket
}

resource "aws_s3_bucket_policy" "example_config_bucket_policy" {
  bucket = aws_s3_bucket.example_config_bucket.id
  policy = data.aws_iam_policy_document.config_bucket_policy.json
}

resource "aws_s3_bucket_server_side_encryption_configuration" "config_bucket_enc" {
  bucket = aws_s3_bucket.example_config_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "config_bucket_lifecycle" {
  bucket = aws_s3_bucket.example_config_bucket.id

  rule {
    id = "retention"
    expiration {days = 365}
    status = "Enabled"
  }
}

data "aws_iam_policy_document" "config_bucket_policy" {
  statement {
    sid    = "AWSConfigBucketPermissionsCheck"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["config.amazonaws.com"]
    }

    actions   = ["s3:GetBucketAcl"]
    resources = [aws_s3_bucket.example_config_bucket.arn]
  }
  statement {
    sid    = "AWSConfigBucketDelivery"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["config.amazonaws.com"]
    }
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.example_config_bucket.arn}/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}