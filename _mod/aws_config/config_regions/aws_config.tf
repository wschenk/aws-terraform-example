resource "aws_config_configuration_recorder" "config_default" {
  name     = var.aws_config_name
  role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig"
  recording_group {
    all_supported = true
    include_global_resource_types = true
  }
}

resource "aws_config_delivery_channel" "default" {
  name = "default"
  s3_bucket_name = var.config_bucket
  snapshot_delivery_properties {delivery_frequency = "TwentyFour_Hours"}
  depends_on = [aws_config_configuration_recorder.config_default]

}

