variable "config_bucket" {
    type = string
}

variable "aws_config_name" {
  type    = string
  default = "default"
}
