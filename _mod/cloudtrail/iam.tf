
resource "aws_iam_role" "cloudtrail_cloudwatch_role" {
  name               = "CloudTrail_CloudWatchLogs_Role"
  assume_role_policy = data.aws_iam_policy_document.cloudtrail_assume_role_policy.json
}

resource "aws_iam_policy" "cloudtrail_cloudwatch_policy" {
  name        = "CloudTrail-Cloudwatch"
  description = "Policy to grant Cloudtrail access to Cloudwatch LogGroup"
  policy      = data.aws_iam_policy_document.cloudtrail_cloudwatch_policy.json
}

resource "aws_iam_policy" "cloudtrail_s3_policy" {
  name        = "CloudTrail-S3"
  description = "Policy to grant Cloudtrail access to S3 Bucket"
  policy      = data.aws_iam_policy_document.cloudtrail_s3_policy.json
}

resource "aws_iam_role_policy_attachment" "attach_cloudtrail_cloudwatch" {
  role       = aws_iam_role.cloudtrail_cloudwatch_role.name
  policy_arn = aws_iam_policy.cloudtrail_cloudwatch_policy.arn
}

resource "aws_iam_role_policy_attachment" "attach_cloudtrail_s3" {
  role       = aws_iam_role.cloudtrail_cloudwatch_role.name
  policy_arn = aws_iam_policy.cloudtrail_s3_policy.arn
}

data "aws_iam_policy_document" "cloudtrail_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudtrail_s3_policy" {
  statement {
    sid    = "AWSCloudTrailImportBucketAccess"
    effect = "Allow"
    actions = [
      "s3:ListBucket",
    "s3:GetBucketAcl"]
    resources = [aws_s3_bucket.example_cloudtrail_bucket.arn]
  }
  statement {
    sid     = "AWSCloudTrailImportGetObject"
    effect  = "Allow"
    actions = ["s3:GetObject"]
    resources = [
      "${aws_s3_bucket.example_cloudtrail_bucket.arn}/AWSLogs/${data.aws_caller_identity.current.account_id}/CloudTrail",
      "${aws_s3_bucket.example_cloudtrail_bucket.arn}/AWSLogs/${data.aws_caller_identity.current.account_id}/CloudTrail/*"
    ]
  }
}

data "aws_iam_policy_document" "cloudtrail_cloudwatch_policy" {
  statement {
    sid    = "AWSCloudTrailPermissions"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["${aws_cloudwatch_log_group.cloudtrail_loggroup.arn}:*"]
  }
}
