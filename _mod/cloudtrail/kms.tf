resource "aws_kms_key" "cloudtrail" {
  description             = "KMS for cloudtrail"
  deletion_window_in_days = 7
  is_enabled              = true
  enable_key_rotation     = true

}

resource "aws_kms_alias" "cloudtrail_alias" {
  name          = "alias/st-cloudtrail"
  target_key_id = aws_kms_key.cloudtrail.key_id
}

resource "aws_kms_key_policy" "cloudtrail_kms" {
  key_id = aws_kms_key.cloudtrail.key_id
  policy = jsonencode({
    Id = "cloudtrail"
    Statement = [
      {
        "Sid"    = "Enable IAM user permissions",
        "Effect" = "Allow",
        "Principal" = {
          "AWS" = [
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
          ]
        },
        "Action"   = "kms:*",
        "Resource" = "*"
      },
      {
        "Sid"    = "Allow CloudTrail to encrypt logs",
        "Effect" = "Allow",
        "Principal" = {
          "Service" = "cloudtrail.amazonaws.com"
        },
        "Action"   = "kms:GenerateDataKey*",
        "Resource" = "*",
        "Condition" = {
          "StringEquals" = {
            "AWS:SourceArn" = "arn:aws:cloudtrail:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:trail/org-trail"
          },
          "StringLike" = {
            "kms:EncryptionContext:aws:cloudtrail:arn" = "arn:aws:cloudtrail:*:${data.aws_caller_identity.current.account_id}:trail/*"
          }
        }
      },
      {
        "Sid"    = "Allow CloudTrail to describe key",
        "Effect" = "Allow",
        "Principal" = {
          "Service" = "cloudtrail.amazonaws.com"
        },
        "Action"   = "kms:DescribeKey",
        "Resource" = "*"
      },
      {
        "Sid"    = "Allow principals in the account to decrypt log files",
        "Effect" = "Allow",
        "Principal" = {
          "AWS" = "*"
        },
        "Action" = [
          "kms:Decrypt",
          "kms:ReEncryptFrom"
        ],
        "Resource" = "*",
        "Condition" = {
          "StringEquals" = {
            "kms:CallerAccount" = data.aws_caller_identity.current.account_id
          },
          "StringLike" = {
            "kms:EncryptionContext:aws:cloudtrail:arn" = "arn:aws:cloudtrail:*:${data.aws_caller_identity.current.account_id}:trail/*"
          }
        }
      },
      {
        "Sid"    = "Allow alias creation during setup",
        "Effect" = "Allow",
        "Principal" = {
          "AWS" = "*"
        },
        "Action"   = "kms:CreateAlias",
        "Resource" = "*",
        "Condition" = {
          "StringEquals" = {
            "kms:ViaService"    = "ec2.${data.aws_region.current.name}.amazonaws.com",
            "kms:CallerAccount" = data.aws_caller_identity.current.account_id
          }
        }
      },
      {
        "Sid"    = "Enable cross account log decryption",
        "Effect" = "Allow",
        "Principal" = {
          "AWS" = "*"
        },
        "Action" = [
          "kms:Decrypt",
          "kms:ReEncryptFrom"
        ],
        "Resource" = "*",
        "Condition" = {
          "StringEquals" = {
            "kms:CallerAccount" = data.aws_caller_identity.current.account_id
          },
          "StringLike" = {
            "kms:EncryptionContext:aws:cloudtrail:arn" = "arn:aws:cloudtrail:*:${data.aws_caller_identity.current.account_id}:trail/*"
          }
        },
      }
    ]
    Version = "2012-10-17"
  })
}
