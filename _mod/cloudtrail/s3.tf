 resource "aws_s3_bucket" "example_cloudtrail_bucket" {
  bucket     = var.cloudtrail_bucket
}

resource "aws_s3_bucket_policy" "example_cloudtrail_bucket_policy" {
  bucket = aws_s3_bucket.example_cloudtrail_bucket.id
  policy = data.aws_iam_policy_document.cloudtrail_org_bucket_policy.json
}

resource "aws_s3_bucket_server_side_encryption_configuration" "cloudtrail_bucket_enc" {
  bucket = aws_s3_bucket.example_cloudtrail_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

data "aws_iam_policy_document" "cloudtrail_org_bucket_policy" {
  statement {
    sid    = "AWSCloudTrailAclCheck"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    actions   = ["s3:GetBucketAcl"]
    resources = ["arn:aws:s3:::${var.cloudtrail_bucket}"]
  }
  statement {
    sid    = "AWSCloudTrailWrite"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${var.cloudtrail_bucket}/AWSLogs/${data.aws_caller_identity.current.account_id}/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
  statement {
    sid    = "AWSCloudTrailWriteOrg"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${var.cloudtrail_bucket}/AWSLogs/${data.aws_organizations_organization.example_org.id}/*"]
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}