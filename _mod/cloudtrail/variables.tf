variable "cloudtrail_bucket" {
  type        = string
  description = "S3 bucket for for account cloudtrail logs"
  default     = "example-org-cloudtrail"
}

variable "cloudtrail_name" {
  type        = string
  description = "Name of the AWS trail"
  default     = "org-trail"
}

variable "aws_region" {
  type        = string
  description = "AWS region to apply change in"
  default     = "us-east-1"
}
