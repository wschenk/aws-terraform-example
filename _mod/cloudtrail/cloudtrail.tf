resource "aws_cloudtrail" "example_cloudtrail" {
  name                       = var.cloudtrail_name
  s3_bucket_name             = var.cloudtrail_bucket
  is_multi_region_trail      = true
  is_organization_trail      = true
  enable_log_file_validation = true
  kms_key_id                 = aws_kms_key.cloudtrail.arn
  cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.cloudtrail_loggroup.arn}:*"
  cloud_watch_logs_role_arn  = aws_iam_role.cloudtrail_cloudwatch_role.arn  
  depends_on                 = [aws_cloudwatch_log_group.cloudtrail_loggroup,aws_s3_bucket_policy.example_cloudtrail_bucket_policy,aws_kms_key.cloudtrail]
}

resource "aws_cloudwatch_log_group" "cloudtrail_loggroup" {
  name = "Cloudtrail/DefaultLogGroup"
}
