# Define security hub admin account as org master account
resource "aws_securityhub_organization_admin_account" "example_org_admin" {
  depends_on       = [aws_securityhub_account.example]
  admin_account_id = data.aws_caller_identity.current.account_id
}

resource "aws_securityhub_account" "example" {}


# Auto enable security hub in organization member accounts
resource "aws_securityhub_organization_configuration" "example_org_security_hub" {
  auto_enable = true
}


# Aggregate findings to one region
resource "aws_securityhub_finding_aggregator" "example" {
  linking_mode = "ALL_REGIONS"
}

resource "aws_securityhub_standards_subscription" "cis_aws_foundations_benchmark_v4" {
  standards_arn = "arn:${data.aws_partition.current.partition}:securityhub:${data.aws_region.current.name}::standards/cis-aws-foundations-benchmark/v/1.4.0"
}

